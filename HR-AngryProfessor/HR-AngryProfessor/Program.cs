﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR_AngryProfessor
{
    class Program
    {
        static void Main(string[] args)
        {
            var angryProfessor=new AngryProfessor();
            Console.WriteLine("Enter the Number of TestCases");
            var numberOfTestCases=angryProfessor.AcceptNumberOfTestCases(Convert.ToInt32(Console.ReadLine()));
            angryProfessor.GenerateOutput(numberOfTestCases);
            Console.ReadLine();
        }
    }
}
