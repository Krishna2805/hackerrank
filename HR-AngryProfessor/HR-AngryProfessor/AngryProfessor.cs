﻿namespace HR_AngryProfessor
{
    using System;

    class AngryProfessor
    {
        private int _numberOfTestCases;
        private string _typeOfTests;
        private string[] _strings;
        private int _numberOfStudents;
        private int[] _numberOfStudentsToPass;
        private string[] _acceptEachTime;
        private int[] _count;
        private int[] _value;


        internal void GenerateOutput(int input)
        {
            CalculateEligibility(input);
        }

        private void CalculateEligibility(int input)
        {
            _count = new int[input];
            _numberOfStudentsToPass = new int[input]; try
            {
                for (int i = 0; i < input; i++)
                {
                    _typeOfTests = Convert.ToString(Console.ReadLine());
                    _strings = _typeOfTests.Split(' ');
                    _numberOfStudents = Convert.ToInt32(_strings[0]);
                    for (int k = 0; k < input; k++)
                    {
                        _numberOfStudentsToPass[i] = Convert.ToInt32(_strings[1]);
                    }
                    var _acceptstudentsTime = Console.ReadLine();
                    
                    if (_acceptstudentsTime != null)
                    {
                        int[] acceptEachTime = Array.ConvertAll(_acceptstudentsTime.Split(' '), Convert.ToInt32);
                        for (int j = 0; j < _numberOfStudents; j++)
                        {
                            _count[i] = (acceptEachTime[j] > 0) ? _count[i] : _count[i] + 1;

                        }
                       
                    }
                }

                for (int i = 0; i < input; i++)
                {
                    Console.WriteLine(_count[i] >= _numberOfStudentsToPass[i] ? "NO" : "YES");    
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine("seems somethins is wrong - {0}", ex.Message);
            }


        }

        internal int AcceptNumberOfTestCases(int testInput)
        {
            return testInput;
        }


    }
}
